package ScenarioSpecification
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import org.scenariotools.smlk.*


fun guaranteeScenariosSoS(advertisementService: AdvertisementService,
                          smartphoneApp: SmartphoneApp,
                          electricVehicle: ElectricVehicle,
                          chargingStationOperationService: ChargingStationOperationService,
                          energyInformationService: EnergyInformationService,
                          sosUser: SosUser,
                          gpsSensor: GpsSensor

) = listOf(















    scenario(gpsSensor sends electricVehicle receives ElectricVehicle::positionChanged){
        val loc = it.parameters[0] as Location
        request(electricVehicle sends advertisementService.currentVehicleLocation(loc))
        request(advertisementService sends chargingStationOperationService.getChargingStationsNear(loc))

        val dummyChargingStation1 = ChargingStation("EcoBigCharge")
        val dummyChargingStation2 = ChargingStation("StarCharge")

        val availableChargingStationsEvent = requestParamValuesMightVary(chargingStationOperationService
                sends advertisementService.chargingStationsNear(loc, listOf(dummyChargingStation1, dummyChargingStation2)))

        // To be refined: how does the ADService actually collect the price updates to forward to the user?

        val advertisement = Advertisement(mapOf(dummyChargingStation1 to 295, dummyChargingStation2 to 289))
        requestParamValuesMightVary(advertisementService sends smartphoneApp.advertismentInformation(advertisement))
        request(smartphoneApp sends sosUser.showAdvertisement())
    },

    // Advertisement Service CS Szenario.
    scenario(electricVehicle sends AdvertisementService::currentVehicleLocation.symbolicEvent()){
        val loc = it.parameters[0] as Location

        request(advertisementService sends chargingStationOperationService.getChargingStationsNear(loc))

        val availableChargingStationsEvent = waitFor(chargingStationOperationService
                sends AdvertisementService::chargingStationsNear.symbolicEvent())
        val availableChargingStations = availableChargingStationsEvent.parameters[1] as List<ChargingStation>

        val chargingStationsCurrentPrice = mutableMapOf<ChargingStation, Int>()

        scenario {
            for(chargingStation in availableChargingStations){
                request(advertisementService sends chargingStation.getCurrentKWHPrice())
                val replyEvent = waitFor(chargingStation sends AdvertisementService::updateCurrentKWHPrice.symbolicEvent())
                chargingStationsCurrentPrice.put(chargingStation, replyEvent.parameters[1] as Int)
            }
        } before (advertisementService sends smartphoneApp receives SmartphoneApp::advertismentInformation)
        forbiddenEvents.clear()
        val advertisement = Advertisement(chargingStationsCurrentPrice)
        request(advertisementService sends smartphoneApp.advertismentInformation(advertisement))
    },

    // Charging Station CS Scenario.
    scenario(advertisementService sends ChargingStation::getCurrentKWHPrice.symbolicEvent()){
        val chargingStation = it.receiver
        request(chargingStation sends advertisementService.updateCurrentKWHPrice(chargingStation, chargingStation.currentKWhPrice))
    }























)

fun createScenarioProgram(advertisementService: AdvertisementService,
                          smartphoneApp: SmartphoneApp,
                          electricVehicle: ElectricVehicle,
                          chargingStationOperationService: ChargingStationOperationService,
                          energyInformationService: EnergyInformationService,
                          sosUser: SosUser,
                          gpsSensor: GpsSensor) : ScenarioProgram{



    chargingStationOperationService.chargingStations.add(ChargingStation("EcoChargeXL", 293))
    chargingStationOperationService.chargingStations.add(ChargingStation("HeroCharge", 288))

    val scenarioProgram = ScenarioProgram(
        "Scenario Specification SoS",
        isControlledObject = { o : Any -> o is Rps},
        eventNodeInputBufferSize = 25)

    scenarioProgram.addEnvironmentMessageType(
        electricVehicle::positionChanged
        //rps::calculateRoute
    )

    scenarioProgram.activeGuaranteeScenarios.addAll(
        guaranteeScenariosSoS(
            advertisementService, smartphoneApp, electricVehicle, chargingStationOperationService, energyInformationService, sosUser, gpsSensor
        )
    )

    return scenarioProgram
}