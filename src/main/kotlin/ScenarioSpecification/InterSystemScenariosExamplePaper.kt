package ScenarioSpecification
import org.scenariotools.smlk.ScenarioProgram
import org.scenariotools.smlk.receives
import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.sends


fun guaranteeScenariosSoS(user: SosUser,
                          csos: ChargingStationOperatorService,
                          app: App,
                          rps: Rps

) = listOf(

    scenario(user sends app receives App::addTravelPreferences){
        val fromLoc =it.parameters[0] as String
        val toLoc =it.parameters[1] as String
        request(app sends rps.calculateRoute(fromLoc, toLoc))
        requestParamValuesMightVary(rps sends app.calculateRouteResponse(Route(listOf())))
        request(app.optimizeRoute())
        request(app sends user.showMapWithOptimizedRoute())
    },
    scenario(user sends app receives App::addTravelPreferences){
        scenario {
            request(app sends csos.chargingStationGpsDataRequest())
//            requestParamValuesMightVary(
//                csos sends app.considerChargingStationLocations(
//                    ChargingStation(listOf())
//                )
//            )
        }.before(app.optimizeRoute())
    }
)

fun createScenarioProgram(user: SosUser,
                          csos: ChargingStationOperatorService,
                          app: App,
                          rps: Rps) : ScenarioProgram{

    val scenarioProgram = ScenarioProgram(
        "Scenario Specification SoS",
        isControlledObject = { o : Any -> o is Rps},
        eventNodeInputBufferSize = 25)

    scenarioProgram.addEnvironmentMessageType(
        app::addTravelPreferences,
        //app::calculateRouteResponse
        //rps::calculateRoute
    )

    scenarioProgram.activeGuaranteeScenarios.addAll(
        guaranteeScenariosSoS(
            user, csos, app, rps
        )
    )

    return scenarioProgram
}