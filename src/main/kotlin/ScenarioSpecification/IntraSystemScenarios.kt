package ScenarioSpecification

import org.scenariotools.smlk.*

fun guaranteeScenariosAdvertisementService(chargingInfrastructInterface: ChargingInfrastructInterface,
                                           userDeviceInterface: UserDeviceInterface,
                                           advertisementService: AdvertisementService,
                                           advertisementOptimizer: AdvertisementOptimizer
                                           ) = listOf(


    scenario(chargingInfrastructInterface sends advertisementService receives AdvertisementService::chargingStationsNear){
        val chargingStations = it.parameters[0] as ChargingStation
        request(advertisementOptimizer.evaluateEnergyPriceInformation(chargingStations))
        request(advertisementOptimizer.evaluateShareOfRenewables(chargingStations))
        val advertisement = createAdvertisement()
        request(advertisementService sends userDeviceInterface.advertismentInformation(advertisement))
    }



)