package ScenarioSpecification

import org.scenariotools.smlk.event

data class Location(val longitude: Double, val latitude: Double)
data class Route( val locations: List<Location>)

data class Advertisement(val chargingStationsCurrentPrice : Map<ChargingStation, Int> = mutableMapOf())
/*
*
* Inter-System Level
*
* */
class SosUser{
    var start = " "
    var destination = " "
    fun addTravelPreferences(start: String, destination: String) = event(start, destination){
        this.start = start
        this.destination = destination
    }
    fun showMapWithOptimizedRoute() = event{}
    fun showAdvertisement() = event{}
    fun showUpdatedAdvertisement() = event{}
}
open class RouteReceiver{
    var currentRoute = Route(listOf())
    fun calculateRouteResponse(route: Route) = event(route){
        this.currentRoute = route}
}
class Rps{
    var start = " "
    var destination = " "

    fun calculateRoute(start : String, destination : String) = event(start, destination){
        this.start = start
        this.destination = destination
    }
}
open class ChargingInfrastructInterface{
    //var currentPosition
    //fun calculateRouteResponse(route: Route) = event(route){
    //    this.currentRoute = route}
}
open class UserDeviceInterface{
    fun advertismentInformation(advertisement: Advertisement) = event(advertisement){}
}

class App:RouteReceiver(){
    var start = " "
    var destination = " "
    //var chargingStations = ChargingStation(listOf())
    var soc = 0.0
    var listOfRecommendations = " "

    //fun considerChargingStationLocations(chargingStation:ChargingStation) = event(chargingStation){this.chargingStations = chargingStation}
    fun optimizeRoute() = event{}
    fun addTravelPreferences(start:String, destination:String) = event(start,destination){
        this.start = start
        this.destination = destination
    }
    fun updateAppVersion() = event(){}
    fun sendUpdateNotification() = event(){}
}
class ChargingStationOperatorService {
    fun chargingStationGpsDataRequest() = event{}
}

class GpsSensor(){

}


class SmartphoneApp: UserDeviceInterface(){
    fun displayAdvertisement() = event{}
}
class ElectricVehicle(){
    fun positionChanged(location: Location) = event(location){}
}
class ChargingStationOperationService: ChargingInfrastructInterface(){
    val chargingStations = mutableListOf<ChargingStation>()
    fun getChargingStationsNear(location: Location) = event(location){}
    fun shareOfRenewablesForAvailableChargingStations() = event{}
    fun energyPriceChanged() = event{}
    fun shareOfRenewablesChanged() = event{}
}
class EnergyInformationService(){

}
class AdvertisementService(){

    val currentKHWPrice = mutableMapOf<ChargingStation,Int>()

    fun currentVehicleLocation(location: Location) = event(location){}
    fun chargingStationsNear(location: Location, chargingStations : List<ChargingStation>) = event(location, chargingStations){}
    fun energyPriceForAvailableChargingStations() = event{}
    fun updateCurrentKWHPrice(chargingStation: ChargingStation, priceCents: Int) = event(chargingStation, priceCents){
        currentKHWPrice[chargingStation] = priceCents
    }
}

class ChargingStation (val name : String, var currentKWhPrice : Int = 0){
    override fun toString() = name
    fun getCurrentKWHPrice() = event(){}
}

/*
*
* Intra-System Level
*
* */

// Rps Components
class GpsService{
    fun getLocations(fromLocString:String, toLocString:String) = event(fromLocString,toLocString){}
}
class RpsController{
    fun locations(start : Location, destination : Location) = event(start, destination){}
    fun calculatedRoute(route:Route) = event(route){
    }
}
class  RoutePlaner{
    fun calculateRoute(start : Location, destination : Location) = event(start, destination){}
}

val rpsController = RpsController()
val gpsService = GpsService()
val routePlaner = RoutePlaner()

//Advertisement
class AdvertisementOptimizer{
    fun evaluateEnergyPriceInformation(chargingStation: ChargingStation) = event(chargingStation){}
    fun evaluateShareOfRenewables(chargingStation: ChargingStation) = event{chargingStation}
    fun createAdvertisement() = event{}
}
fun createAdvertisement(): Advertisement{
    return Advertisement(mapOf(ChargingStation("EcoStar")  to 286))
}