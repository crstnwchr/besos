@advertisement
Feature: send notice with available charging stations near vicinity

  Background: init test setup
    Given init test setup

  Scenario: "Electric Vehicle" changes position
    When the "Electric Vehicle" changes its position
    Then the "Advertisement SoS" offers available charging stations in the closer vicinity

#  Scenario: energy price changed
#    When the energy price changed
#    Then the "Advertisement SoS" provides an updated advertisement for the charging stations in the closer vicinity
#
#  Scenario: share of renewables changed
#    When the share of renewable energy changed
#    Then the "Advertisement SoS" provides an updated advertisement for the charging stations in the closer vicinity