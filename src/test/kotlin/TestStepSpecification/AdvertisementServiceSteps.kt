package TestStepSpecification
import ScenarioSpecification.Location
import TestConfiguration.*
import io.cucumber.java8.En
import org.scenariotools.smlk.sends

class AdvertisementServiceSteps : En {
    init {
        When("the \"Electric Vehicle\" changes its position") {
            trigger(gpsSensor sends electricVehicle.positionChanged(Location(23232.323, 323.2323)))
        }
        Then("the \"Advertisement SoS\" offers available charging stations in the closer vicinity") {
            eventually(smartphoneApp sends sosUser.showAdvertisement())
        }
        When("the energy price changed") {
            trigger(energyInformationService sends chargingStationOperationService.energyPriceChanged())
        }
        Then("the \"Advertisement SoS\" provides an updated advertisement for the charging stations in the closer vicinity") {
            eventually(smartphoneApp sends sosUser.showUpdatedAdvertisement())
        }
        When("the share of renewable energy changed") {
            trigger(energyInformationService sends chargingStationOperationService.shareOfRenewablesChanged())
        }
    }
}