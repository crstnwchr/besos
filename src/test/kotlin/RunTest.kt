
import io.cucumber.junit.Cucumber
import io.cucumber.junit.CucumberOptions
import org.junit.runner.RunWith

@RunWith(Cucumber::class)
@CucumberOptions(
    features = ["src/main/kotlin/FeatureSpecification/"],
    //tags = ["@SoSTest"],
    //tags = ["@CsTest"],
    strict = true
)

class RunTest
