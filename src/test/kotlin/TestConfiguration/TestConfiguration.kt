package TestConfiguration

import ScenarioSpecification.*
import org.scenariotools.smlk.EventNode
import ScenarioSpecification.createScenarioProgram
import io.cucumber.java8.En
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.scenariotools.smlk.*

val advertisementService = AdvertisementService()
val smartphoneApp = SmartphoneApp()
val electricVehicle = ElectricVehicle()
val chargingStationOperationService = ChargingStationOperationService()
val energyInformationService = EnergyInformationService()
val sosUser = SosUser()
val gpsSensor = GpsSensor()

var scenarioSpecificationSoS = createScenarioProgram(
    advertisementService, smartphoneApp, electricVehicle, chargingStationOperationService, energyInformationService, sosUser, gpsSensor)


//var scenarioSpecificationRps = createScenarioProgram(
//    app,
//    rps
//)

var testEventNode = EventNode()



class testSteps : En {
    init {
        Given("init test setup") {
            testEventNode = EventNode()
            scenarioSpecificationSoS = createScenarioProgram(advertisementService, smartphoneApp, electricVehicle, chargingStationOperationService, energyInformationService, sosUser, gpsSensor)
            //scenarioSpecificationRps = createScenarioProgram(app, rps)
            scenarioSpecificationSoS.terminatingEvents.add(TestDoneEvent)
            //scenarioSpecificationRps.terminatingEvents.add(TestDoneEvent)

            scenarioSpecificationSoS.eventNode.registerSender(
                testEventNode,
                scenarioSpecificationSoS.environmentMessageTypes.symbolicEvents() union TestDoneEvent
            )
//            scenarioSpecificationSoS.eventNode.registerSender(
//                scenarioSpecificationRps.eventNode,
//                scenarioSpecificationSoS.environmentMessageTypes.symbolicEvents() union TestDoneEvent
//            )
//            scenarioSpecificationRps.eventNode.registerSender(
//                scenarioSpecificationSoS.eventNode,
//                scenarioSpecificationRps.environmentMessageTypes.symbolicEvents() union TestDoneEvent
//            )

            val observedTesteeEventsSoS = ALLEVENTS excluding scenarioSpecificationSoS.environmentMessageTypes.symbolicEvents()
//            val observedTesteeEventsCs = ALLEVENTS excluding scenarioSpecificationRps.environmentMessageTypes.symbolicEvents()

            testEventNode.registerSender(
                scenarioSpecificationSoS.eventNode,
                observedTesteeEventsSoS excluding TestDoneEvent
            )
//            testEventNode.registerSender(
//                scenarioSpecificationRps.eventNode,
//                observedTesteeEventsCs excluding TestDoneEvent
//            )


            runBlocking() {
                GlobalScope.launch {
                    scenarioSpecificationSoS.run()
                }
//                GlobalScope.launch {
//                    scenarioSpecificationRps.run()
//                }
            }
        }


    }
}
